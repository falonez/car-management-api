'use strict';
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return await queryInterface.bulkInsert('users', [{
      user_name: 'akses@mail.com',
      password: await bcrypt.hash('password',7),
      full_name: 'Akses Super',
      token: await jwt.sign({user_name: 'akses@mail.com',password: bcrypt.hash('getcloser',7),full_name: 'Akses Super'},'Supersecret'),
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
