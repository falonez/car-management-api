# Binar API Management

Api Menggunakan Design Pattern Service Repository

## Entity Relationship Diagram (ERD)
ERD dari sistem ini bisa dilihat melalui tautan berikut : https://dbdiagram.io/d/63538bcb4709410195b96c5b

## Installation
Buat .env berdasarkan Env-exampe dan jalankan sintaks berikut :

```bash
npm i
npx sequelize db:create
npx sequelize db:migrate
npx sequelize db:seed:all
```

## Swagger

Akses Swagger Open API Doc = ```http://hostname:port/api-docs```

## API Endpoint
Login Superadmin :

```javascript
Username : ```akses@mail.com```

Password : ```password```

Token Prefix : ```Supersecret generatedjwttoken```
```

Hak Akses dapat di temukan di dalam tabel user di dalam database yang sudah di seed
