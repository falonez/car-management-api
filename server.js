 const express = require("express");
 const morgan = require("morgan");
 const router = require("./config/routes");
 
 const app = express();
 const port = 3000;
 
 /** Install request logger */
 app.use(morgan("dev"));
 
 /** Install JSON request parser */
 app.use(express.json());
 
 /** Install Router */
 app.use(router);

 app.listen(port, () => {
    console.log(`Listening on http://localhost:${port}`)
  })
 